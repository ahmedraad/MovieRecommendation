//
//  MButton.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class MButton: UIButton {
    
    @IBInspectable var isCircle: Bool = true
    @IBInspectable var color: UIColor = .primary
    @IBInspectable var image_inset: CGFloat = 5
    
    var animate: Bool = false
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    func setup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        if isCircle {
            self.layer.cornerRadius = self.frame.width / 2
            self.clipsToBounds = true
        }
        self.backgroundColor = color
        self.imageEdgeInsets = UIEdgeInsets(top: image_inset, left: image_inset, bottom: image_inset, right: image_inset)
        self.imageView?.contentMode = .scaleAspectFit
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.transform = .identity
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.transform = .identity
            }
        }
    }
}

