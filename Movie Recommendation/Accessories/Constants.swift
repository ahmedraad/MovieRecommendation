//
//  Constants.swift
//  Linkaai
//
//  Created by Ahmed Raad on 5/29/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD
import SwiftMessages
import CoreLocation

extension UIColor {
    @nonobjc static var primary: UIColor = UIColor(hexString: "FF5B66")
    @nonobjc static var darkWhite: UIColor = UIColor(hexString: "282828")
    @nonobjc static var accent: UIColor = UIColor(hexString: "4c4c4c")
    @nonobjc static var lightPrimary: UIColor = UIColor(hexString: "1F2537")
    @nonobjc static var background: UIColor = UIColor(hexString: "181C28")
}

class Utility {
    
    class func convertCLLocationDistanceToKiloMeters (_ targetDistance : CLLocationDistance?) -> CLLocationDistance {
        guard var targetDistance = targetDistance else {return 0}
        targetDistance =  targetDistance / 1000
        return targetDistance
    }
    
}

let network = Network()

let key = UIApplication.shared.keyWindow!

enum storys: String {
    case Main = "Main",
    Auth = "Auth"
}

func showToast(_ text: String){
//    Toast(text: text).show()
}

var hudInctance = JGProgressHUD(style: .light)

enum fontEnum {
    case light, bold, normal
}

func font(type: fontEnum = .normal, size: CGFloat) -> UIFont {
    switch type {
    case .normal:
        return UIFont(name: "TrebuchetMS", size: size)!
    case .light:
        return UIFont(name: "HelveticaNeue-Light", size: size)!
    case .bold:
        return UIFont(name: "TrebuchetMS-Bold", size: size)!
    }
}

func Board(_ name: storys) -> UIStoryboard {
    return UIStoryboard(name: name.rawValue, bundle: nil)
}

var isX: Bool {
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 2436:
            return true
        default:
            return false
        }
    } else {
        return false
    }
}

extension UITableView {
    func registernib(_ n: String){
        self.register(UINib(nibName: n, bundle: nil), forCellReuseIdentifier: n)
    }
    func scrollToBottom(animated: Bool) {
        let y = contentSize.height - frame.size.height
        setContentOffset(CGPoint(x: 0, y: (y<0) ? 0 : y), animated: animated)
    }
}

extension UICollectionView {
    func registerNib(_ name: String) {
        self.register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
    }
}


extension UIViewController {
    
    func initial(_ color: UIColor = .background) {
        self.view.backgroundColor = color
    }
    
    func showMessage(_ text: String, theme: Theme = .error, position: SwiftMessages.PresentationStyle){
        let view = MessageView.viewFromNib(layout: .cardView)
        view.configureTheme(theme)
        view.titleLabel?.isHidden = true
        view.button?.isHidden = true
        view.configureContent(body: text)
        var config = SwiftMessages.Config()
        config.presentationStyle = position
        config.duration = .seconds(seconds: 1.3)
        config.interactiveHide = true
        SwiftMessages.show(config: config, view: view)
    }
    
    func showNetworkMessage(_ text: String) {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureTheme(backgroundColor: .red, foregroundColor: .white)
        view.configureContent(title: text, body: text)
        var config = SwiftMessages.Config()
        config.presentationContext = .window(windowLevel: UIWindowLevelStatusBar)
        config.duration = .seconds(seconds: 1.3)
        config.interactiveHide = true
        SwiftMessages.show(config: config, view: view)
    }
    
    func hud(_ text: String = "Processing", stopFirst: Bool = true, theme: JGProgressHUDStyle = .light){
//        if stopFirst { self.stophud() }
//        if theme == .dark {
//            hudInctance = JGProgressHUD(style: .dark)
//            hudInctance.interactionType = .blockTouchesOnHUDView
//        } else {
//            hudInctance = JGProgressHUD(style: .light)
//        }
//        hudInctance.textLabel.text = text
//        hudInctance.interactionType = .blockTouchesOnHUDView
//        hudInctance.parallaxMode = .device
//        hudInctance.show(in: self.view, animated: true)
        self.pleaseWait()
    }
    
    func stophud(){
//        hudInctance.dismiss(afterDelay: 0.4, animated: true)
        self.clearAllNotice()
    }
    
    func viewWithNib(_ named: String) -> UIView{
        let nib = UINib(nibName: named, bundle: Bundle(for: type(of: self)))
        if let header  = nib.instantiate(withOwner: nil, options: nil).first as? UIView {
            return header
        }
        return UIView()
    }
}

extension UITableViewCell {
    func viewWithNib(_ named: String) -> UIView{
        let nib = UINib(nibName: named, bundle: Bundle(for: type(of: self)))
        if let header  = nib.instantiate(withOwner: nil, options: nil).first as? UIView {
            return header
        }
        return UIView()
    }
}

func delay(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

extension UINavigationController {
    func setNavigation(){
        self.navigationBar.isTranslucent = false
        self.navigationBar.barStyle = .black
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.barTintColor = .primary
        self.navigationBar.tintColor = .white
        UIApplication.shared.statusBarStyle = .lightContent
    }
}
extension UIView
{
    func addBlurEffect()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

// Fields Validations

func replace(org: String, repText: String, with: String) -> String {
    if let range = org.range(of: repText) {
        return org.replacingOccurrences(of: repText, with: with, options: .regularExpression, range: range)
    }
    return ""
}

extension String {
    
    func replace(this text: String, with: String) -> String {
        if let range = self.range(of: text) {
            return self.replacingOccurrences(of: text, with: with, options: .regularExpression, range: range)
        }
        return ""
    }
    
    var toInt: Int {
        return Int(self) ?? 0
    }
}

extension URL {
    
    func absoluteStringByTrimmingQuery() -> String? {
        if var urlcomponents = URLComponents(url: self, resolvingAgainstBaseURL: false) {
            urlcomponents.query = nil
            return urlcomponents.string
        }
        return nil
    }
}

