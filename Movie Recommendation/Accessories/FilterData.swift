//
//  FilterData.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/19/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

final class FilterData {
    
    static var yearsData = ""
    static var ratingData = ""
    static var keywords: [[String: Any]] = []
    static var movies: [[String: Any]] = []
    
    func appendYear(_ value: String) {
        FilterData.yearsData.append(value + ",")
    }
    
    func appendRate(_ value: String) {
        FilterData.ratingData.append(value + ",")
    }
    
    func appendKeywords(_ value: [String: Any]) {
        UD().getUserData().forEach {
            if let status = $0.isSelected {
                if status {
                    FilterData.keywords.append(["type": "tag", "liked": true, "id": "\($0.id)"])
                }
            }
        }
        FilterData.keywords.append(value)
    }
    
    
    var getFilter: String {
        return FilterData.yearsData + FilterData.ratingData
    }
    
    var getKeywordFilter: [[String: Any]] {
        return FilterData.keywords
    }
    
}
