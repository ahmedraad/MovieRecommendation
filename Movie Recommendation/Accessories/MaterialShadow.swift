//
//  MaterialShadow.swift
//  FTCOM
//
//  Created by Ahmed Raad on 7/19/16.
//  Copyright © 2016 Ahmed Raad. All rights reserved.
//

import UIKit

@IBDesignable
class MaterialShadow: UIView {
    
    @IBInspectable var corner_Radius: CGFloat = 0
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 1
    @IBInspectable var shadow_Color: UIColor? = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.6)
    @IBInspectable var shadow_Opacity: Float = 0.1
    
    override func layoutSubviews() {
        layer.cornerRadius = corner_Radius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: corner_Radius)
        layer.masksToBounds = false
        layer.shadowColor = shadow_Color?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadow_Opacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}

