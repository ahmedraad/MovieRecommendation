//
//  MovieButton.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/18/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit

class MovieButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    func setup() {
        backgroundColor = isEnabled ? .primary : .lightGray
        layer.cornerRadius = 8
        setTitleColor(.white, for: .normal)
    }
}
