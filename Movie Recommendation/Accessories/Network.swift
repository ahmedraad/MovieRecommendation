//
//  Network.swift
//  Linkaai
//
//  Created by Ahmed Raad on 5/29/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import Alamofire

final class Network {
    
    static var publicLink = "https://movix.ai/api/"
    
    enum route: String {
        case recommendations = "recommendations?filter=", movie = "movie/"
    }
    
    func request(type: HTTPMethod = .post ,route: route,parameters: Parameters, movie_id: String = "", isFilter: Bool = false, complete: @escaping (Data) -> ()) {
        var url = Network.publicLink + route.rawValue
        if isFilter {
            url = ""
            url = Network.publicLink + route.rawValue + FilterData().getFilter
        }
        if route == .movie {
            url = ""
            url = Network.publicLink + route.rawValue + movie_id
        }
        Alamofire.request(url, method: type, parameters: parameters, encoding: ArrayEncoding()).responseData { (data) in
            if let d = data.data {
                complete(d)
            }
        }
    }
    
}



