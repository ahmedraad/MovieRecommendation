//
//  UD.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/18/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

final class UD {
    
    let def = UserDefaults.standard
    
    private var _userDataKey = "USERMOVIESDATAKEY"
    private var _userLikedMoviesKey = "USERLIKEDMOVIESKEY"
    
    func setUserData(_ tag: [Tag]) {
        def.set(try? PropertyListEncoder().encode(tag), forKey: _userDataKey)
    }
    
    func setLikedMovies(_ movie: Movie) {
        let currentMovies = getLikedMovies()
        
        var filterd = currentMovies.filter { (inArrayMovie) -> Bool in
            return inArrayMovie.id != movie.id
        }
        filterd.append(movie)
        def.set(try? PropertyListEncoder().encode(filterd), forKey: _userLikedMoviesKey)
    }
    
    func removeMovie(at index: Int) {
        var currentMovies = getLikedMovies()
        currentMovies.remove(at: index)
        def.set(try? PropertyListEncoder().encode(currentMovies), forKey: _userLikedMoviesKey)
    }
    
    func resetAll() {
        def.set(nil, forKey: _userDataKey)
        def.set(nil, forKey: _userLikedMoviesKey)
    }
    
    func getLikedMovies() -> [Movie] {
        if let data = def.value(forKey: _userLikedMoviesKey) as? Data {
            do {
                return try PropertyListDecoder().decode(Array<Movie>.self, from: data)
            } catch {
                print(error)
                return []
            }
        } else {
            return []
        }
    }
    
    func getUserData() -> [Tag] {
        if let data = def.value(forKey: _userDataKey) as? Data {
            do {
                return try PropertyListDecoder().decode(Array<Tag>.self, from: data)
            } catch {
                print(error)
                return []
            }
        } else {
            return []
        }
    }
    
    func requestPara() -> [[String: Any]] {
        var para: [[String: Any]] = []
        getUserData().forEach {
            if let status = $0.isSelected {
                if status {
                    para.append(["type": "tag", "liked": true, "id": "\($0.id)"])
                }
            }
        }
        return para
    }
    
    func likedMovies() -> [[String: Any]] {
        var para: [[String: Any]] = []
        getLikedMovies().forEach {
            para.append(["type": "movie", "liked": $0.isLiked ?? true, "id": "\($0.id)"])
        }
        return para
    }
    
}
