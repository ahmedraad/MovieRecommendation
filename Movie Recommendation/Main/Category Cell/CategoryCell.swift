//
//  CategoryCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/18/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var bg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bg.backgroundColor = .lightPrimary
        bg.layer.cornerRadius = 5
    }
    
    func configure(_ object: Tag) {
        name.text = object.title
        if let status = object.isSelected {
            if status {
                self.bg.backgroundColor = .primary
            } else {
                self.bg.backgroundColor = .lightPrimary
            }
        } else {
           self.bg.backgroundColor = .lightPrimary
        }
    }

}
