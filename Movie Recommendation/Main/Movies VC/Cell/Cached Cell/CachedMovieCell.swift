//
//  CachedMovieCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/20/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit
import Kingfisher

class CachedMovieCell: UICollectionViewCell {

    @IBOutlet weak var movie_poster: UIImageView!
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var likeStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        movie_poster.layer.cornerRadius = 5
        shadow.layer.cornerRadius = 5
    }

    func configure(object: Movie) {
        let info = KingfisherOptionsInfo(arrayLiteral: .transition(.fade(1)))
        self.movie_poster.kf.setImage(with: object.poster, placeholder: #imageLiteral(resourceName: "placeholder"), options: info)
        if let status = object.isLiked {
            self.likeStatus.image = status ? #imageLiteral(resourceName: "movie_liked") : #imageLiteral(resourceName: "movie_disliked")
        } else {
            self.likeStatus.image = nil
        }
    }
}
