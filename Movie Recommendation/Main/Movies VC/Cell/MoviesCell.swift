//
//  MoviesCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/7/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos
import Lottie

class MoviesCell: UICollectionViewCell {
    
    @IBOutlet weak var movie_poster: UIImageView!
    @IBOutlet weak var movie_title: UILabel!
    @IBOutlet weak var movie_details: UILabel!
    @IBOutlet weak var ratingCount: UILabel!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var likeButton: MButton!
    @IBOutlet weak var dislikeButton: MButton!
    
    let likeAnimation = LOTAnimationView(name: "like")
    let dislikeAnimation = LOTAnimationView(name: "thumb")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        movie_poster.layer.cornerRadius = 5
        movie_poster.clipsToBounds = true
        ratingStar.settings.updateOnTouch = false
        setupLikeDislike()
    }
    
    func setupLikeDislike() {
        likeAnimation.animationSpeed = 1
        likeAnimation.isHidden = true
        likeAnimation.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        likeAnimation.center = movie_poster.center
        contentView.addSubview(likeAnimation)
        
        dislikeAnimation.animationSpeed = 1
        dislikeAnimation.isHidden = true
        dislikeAnimation.frame = CGRect(x: 0, y: 0, width: 300, height: 300)
        dislikeAnimation.center = movie_poster.center
        contentView.addSubview(dislikeAnimation)
    }
    
    func configure(object: Movie) {
        self.movie_title.text = object.title
        let info = KingfisherOptionsInfo(arrayLiteral: .transition(.fade(1)))
        self.movie_poster.kf.indicatorType = .activity
        self.movie_poster.kf.indicator?.startAnimatingView()
        self.movie_poster.kf.setImage(with: object.poster, placeholder: #imageLiteral(resourceName: "placeholder"), options: info, progressBlock: nil) { (_, _, _, _) in
            self.movie_poster.kf.indicator?.stopAnimatingView()
        }
        self.movie_details.text = "\(object.year)"
        self.ratingStar.rating = Double(object.imdb_rating)! / 2
        self.ratingCount.text = object.imdb_rating
    }

}
