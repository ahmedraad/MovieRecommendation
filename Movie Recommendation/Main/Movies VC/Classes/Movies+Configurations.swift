//
//  Movies+Configuration.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/19/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit

extension MoviesVC: UINavigationControllerDelegate {
    
    func setupCollections() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.yearsCollectionView.delegate = self
        self.yearsCollectionView.dataSource = self
        self.keywordsCollectionView.delegate = self
        self.keywordsCollectionView.dataSource = self
        self.cachedMoviesCollection.delegate = self
        self.cachedMoviesCollection.dataSource = self
//        self.navigationController?.delegate = self
        
    }
    
    func setupUI() {
        view.addSubview(filterView)
        filterView.snp.makeConstraints { builder in
            builder.top.equalTo(view).offset(10)
            builder.leading.equalTo(view).offset(10)
            builder.trailing.equalTo(view).offset(-10)
            builder.height.equalTo(60)
        }
        
        filterTap.addTarget(self, action: #selector(didClickOnFilter(_:)))
        filterTapArea.addGestureRecognizer(filterTap)
        
        filterView.addSubview(filterTapArea)
        filterTapArea.snp.makeConstraints { builder in
            builder.top.leading.trailing.equalTo(filterView)
            builder.height.equalTo(70)
        }
        
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { builder in
            builder.top.equalTo(filterView.snp.bottom).offset(10)
            builder.bottom.leading.trailing.equalTo(view)
        }
        setupFilter()
    }
    
    func setupFilter() {
        filterView.addSubview(headLabel)
        headLabel.snp.makeConstraints { builder in
            builder.top.equalTo(10)
            builder.height.equalTo(40)
            builder.leading.equalTo(8)
            builder.trailing.equalTo(-8)
        }
        
        filterView.addSubview(downArrow)
        downArrow.snp.makeConstraints { builder in
            builder.top.equalTo(20)
            builder.trailing.equalTo(-8)
            builder.height.width.equalTo(20)
        }
        
        filterView.addSubview(yearsCollectionView)
        yearsCollectionView.snp.makeConstraints { builder in
            builder.top.equalTo(headLabel.snp.bottomMargin).offset(10)
            builder.leading.equalTo(filterView).offset(5)
            builder.trailing.equalTo(filterView).offset(-5)
            builder.height.equalTo(40)
        }
        
        filterView.addSubview(ratingView)
        ratingView.snp.makeConstraints { builder in
            builder.top.equalTo(yearsCollectionView.snp.bottomMargin).offset(25)
            builder.centerX.equalTo(yearsCollectionView)
            builder.height.equalTo(35)
        }
        
        ratingView.didTouchCosmos = { value in
            switch value {
            case 1.0:
                FilterData.ratingData = "r0-5"
            case 2.0:
                FilterData.ratingData = "r6-7"
            case 3.0:
                FilterData.ratingData = "r7-10"
            default:
                FilterData.ratingData = ""
            }
            self.presenter.getMovies(true)
        }
        
        filterView.addSubview(keywordsCollectionView)
        keywordsCollectionView.snp.makeConstraints { builder in
            builder.top.equalTo(ratingView.snp.bottomMargin).offset(20)
            builder.leading.equalTo(filterView).offset(5)
            builder.trailing.equalTo(filterView).offset(-5)
            builder.height.equalTo(40)
        }
        
        filterView.addSubview(cachedMoviesCollection)
        cachedMoviesCollection.snp.makeConstraints { builder in
            builder.top.equalTo(keywordsCollectionView.snp.bottom).offset(10)
            builder.leading.equalTo(filterView).offset(5)
            builder.trailing.equalTo(filterView).offset(-5)
            builder.height.equalTo(166)
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let transitionThumbnail = zoomTransitionThumbnail, let _ = transitionThumbnail.superview else {
            return nil
        }
        
        if operation == .push {
            zoomTransition = ZoomTransition()
            zoomTransition?.thumbnailFrame = transitionThumbnail.superview!.convert(transitionThumbnail.frame, to: nil)
        }
        zoomTransition?.operation = operation
        
        return zoomTransition
    }
}
