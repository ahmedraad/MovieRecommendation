//
//  Movies+Delegates.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/19/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit

extension MoviesVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MovieViewProtocol {
    
    
    // UICollectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView:
            return movies.count
        case self.yearsCollectionView:
            return years.count
        case self.keywordsCollectionView:
            return keywords.count
        case cachedMoviesCollection:
            return cachedMovies.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCell", for: indexPath) as! MoviesCell
            cell.configure(object: self.movies[indexPath.row])
            cell.likeButton.tag = indexPath.row
            cell.likeButton.addTarget(self, action: #selector(hitLike(_:)), for: .touchUpInside)
            cell.dislikeButton.tag = indexPath.row
            cell.dislikeButton.addTarget(self, action: #selector(hitDislike(_:)), for: .touchUpInside)
            return cell
        case self.yearsCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterTagCell", for: indexPath) as! FilterTagCell
            cell.configure(self.years[indexPath.item])
            return cell
        case self.keywordsCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterTagCell", for: indexPath) as! FilterTagCell
            cell.configure(self.keywords[indexPath.item])
            return cell
        case self.cachedMoviesCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CachedMovieCell", for: indexPath) as! CachedMovieCell
            cell.configure(object: cachedMovies[indexPath.item])
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case self.collectionView:
            var size: CGSize = CGSize()
            if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
                size = CGSize(width: self.view.bounds.width / 5, height: 267)
            } else { size = CGSize(width: self.view.bounds.width / 3, height: 267) }
            return size
        case self.yearsCollectionView:
            return CGSize(width: self.view.bounds.width / 5, height: 35)
        case self.keywordsCollectionView:
            let size = (keywords[indexPath.item].title as NSString).size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)])
            return CGSize(width: size.width * 1.5, height: 35)
        case self.cachedMoviesCollection:
            return CGSize(width: self.view.bounds.width / 3, height: 166)
        default:
            return .zero
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch collectionView {
        case self.collectionView:
            guard let cell = self.collectionView.cellForItem(at: indexPath) as? MoviesCell else {return}
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.showMovieDetail(self.movies[indexPath.item], cell: cell)
        case self.yearsCollectionView:
            if let status = years[indexPath.item].isSelected {
                if status {
                    self.years[indexPath.item].toggle(false)
                } else {
                    self.years[indexPath.item].toggle(true)
                }
            } else {
                self.years[indexPath.item].toggle(true)
            }
            self.yearsCollectionView.reloadData()
            prepareData()
        case self.keywordsCollectionView:
            if let status = keywords[indexPath.item].isSelected {
                if status {
                    self.keywords[indexPath.item].toggle(false)
                } else {
                    self.keywords[indexPath.item].toggle(true)
                }
            } else {
                self.keywords[indexPath.item].toggle(true)
            }
            self.keywordsCollectionView.reloadData()
            prepareData(false)
        case cachedMoviesCollection:
            self.removeMovieFromCache(indexPath)
        default:
            break
        }
    }
    
    func showMovieDetail(_ object: Movie, cell: MoviesCell) {
        let vc = MovieDetailsVC(style: .plain)
        vc.currentMovie = object
        zoomTransitionThumbnail = cell.movie_poster
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func appendMovieInCache(movie: Movie) {
        self.cachedMovies.append(movie)
        self.cachedMoviesCollection.reloadData()
        self.refreshFilterView()
    }
    
    func removeMovieFromCache(_ indexPath: IndexPath) {
        self.cachedMovies.remove(at: indexPath.item)
        UD().removeMovie(at: indexPath.item)
        self.cachedMoviesCollection.reloadData()
        self.refreshFilterView()
        self.presenter.getMovies(true, hasKeywords: true)
    }
    
    @objc func hitLike(_ sender: MButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? MoviesCell {
            cell.likeAnimation.center = cell.movie_poster.center
            cell.likeAnimation.isHidden = false
            cell.likeAnimation.play(completion: { _ in
                cell.likeAnimation.stop()
                cell.likeAnimation.isHidden = true
                var movie = self.movies[indexPath.item]
                movie.isLiked = true
                UD().setLikedMovies(movie)
                self.appendMovieInCache(movie: movie)
                self.presenter.getMovies(true, hasKeywords: true)
            })
        }
    }
    
    @objc func hitDislike(_ sender: MButton) {
        let indexPath = IndexPath(item: sender.tag, section: 0)
        if let cell = collectionView.cellForItem(at: indexPath) as? MoviesCell {
            cell.dislikeAnimation.center = cell.movie_poster.center
            cell.dislikeAnimation.isHidden = false
            cell.dislikeAnimation.play(fromFrame: 5, toFrame: 40, withCompletion: { _ in
                cell.dislikeAnimation.stop()
                cell.dislikeAnimation.isHidden = true
                var movie = self.movies[indexPath.item]
                movie.isLiked = false
                UD().setLikedMovies(movie)
                self.appendMovieInCache(movie: movie)
                self.presenter.getMovies(true, hasKeywords: true)
            })
        }
    }
    
    func prepareData(_ isYears: Bool = true) {
        if isYears {
            FilterData.yearsData = ""
            self.years.forEach {
                if let status = $0.isSelected {
                    if status { FilterData().appendYear($0.value) }
                }
            }
            self.presenter.getMovies(true)
        } else {
            FilterData.keywords.removeAll()
            self.keywords.forEach {
                if let status = $0.isSelected {
                    if status { FilterData().appendKeywords(["type": "tag", "liked": true, "id": "\($0.id)"]) }
                }
            }
            self.presenter.getMovies(true, hasKeywords: true)
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 100 {
            if isFilterExpanded {
                toggleFilter()
            }
        }
    }
    
    
    // Presenter Deleage
    func showHUD() {
        self.hud(stopFirst: false)
    }
    
    func hideHUD() {
        self.stophud()
    }
    
    func setEmptyMovies() {
        self.stophud()
    }
    
    func setMovies(_ movies: [Movie]) {
        self.movies = movies
        self.collectionView.reloadData()
        if let first = movies.first {
            self.keywords = first.tags
            self.keywordsCollectionView.reloadData()
        }
    }
    
    func setCached(_ movies: [Movie]) {
        self.cachedMovies = movies
        self.cachedMoviesCollection.reloadData()
    }
    
    func setYears(_ years: [Year]) {
        self.years = years
        self.yearsCollectionView.reloadData()
    }
}
