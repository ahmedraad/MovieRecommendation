//
//  Movies+Requests.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/19/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class MoviesDataService {
    
    func getList(_ isFilter: Bool = false, hasKeywords: Bool = false, complete: @escaping ([Movie]) -> Void) {
        var fullPara: [[String: Any]] = []
        UD().requestPara().forEach { fullPara.append($0) }
        FilterData().getKeywordFilter.forEach { fullPara.append($0) }
        UD().likedMovies().forEach { fullPara.append($0) }
        network.request(route: .recommendations, parameters: fullPara.asParameters(), isFilter: isFilter) { (d) in
            do {
                let json = try JSONDecoder().decode(Response.self, from: d)
                complete(json.items)
            } catch {
                complete([])
            }
        }
    }
    
    func getYears(complete: @escaping ([Year]) -> Void) {
        guard let path = Bundle.main.url(forResource: "years", withExtension: "plist") else {
            print("Error Reading file")
            return
        }
        do {
            guard let data = try? Data(contentsOf: path) else {
                print("error reading data")
                return
            }
            let json = try PropertyListDecoder().decode([Year].self, from: data)
            complete(json)
        } catch {
            print(error)
        }
    }
    
    func getCachedMovies(complete: @escaping ([Movie]) -> Void) {
        complete(UD().getLikedMovies())
    }
}
