//
//  MoviesViewProtocol.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/20/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

protocol MovieViewProtocol: NSObjectProtocol {
    
    func showHUD()
    func hideHUD()
    func setMovies(_ movies: [Movie])
    func setYears(_ years: [Year])
    func setCached(_ movies: [Movie])
    func setEmptyMovies()
}

class MoviePresenter {
    
    fileprivate let dataService: MoviesDataService
    weak fileprivate var movieView: MovieViewProtocol?
    
    init(dataService: MoviesDataService) {
        self.dataService = dataService
    }
    
    func attachView(_ viewProtocol: MovieViewProtocol) {
        self.movieView = viewProtocol
    }
    
    func detachView() {
        self.movieView = nil
    }
    
    func getMovies(_ isFilter: Bool = false, hasKeywords: Bool = false) {
        movieView?.showHUD()
        dataService.getList(isFilter, hasKeywords: hasKeywords) { [weak self] movies in
            self?.movieView?.hideHUD()
            if movies.count == 0 {
                self?.movieView?.setEmptyMovies()
            } else {
                self?.movieView?.setMovies(movies)
            }
        }
    }
    
    func getYears() {
        dataService.getYears { [weak self] years in
            if years.count == 0 {
                self?.movieView?.setEmptyMovies()
            } else {
                self?.movieView?.setYears(years)
            }
        }
    }
    
    func getCachedMovies() {
        dataService.getCachedMovies { [weak self] movies in
            if movies.count == 0 {
                self?.movieView?.setEmptyMovies()
            } else {
                self?.movieView?.setCached(movies)
            }
        }
    }
}
