//
//  ViewController.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/7/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit
import VegaScrollFlowLayout
import SnapKit
import Alamofire
import Cosmos

class MoviesVC: UIViewController {
    
    let presenter = MoviePresenter(dataService: MoviesDataService())
    
    var cachedMovies = [Movie]()
    var movies = [Movie]()
    var years = [Year]()
    var keywords = [Tag]()
    
    var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .clear
        collection.registerNib("MoviesCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    var cachedMoviesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.showsHorizontalScrollIndicator = false
        collection.backgroundColor = .clear
        collection.registerNib("CachedMovieCell")
        collection.isHidden = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    var yearsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.showsHorizontalScrollIndicator = false
        collection.backgroundColor = .clear
        collection.registerNib("FilterTagCell")
        collection.isHidden = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    var ratingView: CosmosView = {
        let ra = CosmosView()
        ra.translatesAutoresizingMaskIntoConstraints = false
        ra.settings.fillMode = .full
        ra.rating = 0
        ra.settings.totalStars = 3
        ra.settings.starSize = 30
        ra.settings.emptyBorderWidth = 1
        ra.settings.minTouchRating = 0
        ra.settings.filledColor = .white
        ra.settings.emptyBorderColor = .white
        ra.isHidden = true
        return ra
    }()
    
    var keywordsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.showsHorizontalScrollIndicator = false
        collection.backgroundColor = .clear
        collection.registerNib("FilterTagCell")
        collection.isHidden = true
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()
    
    
    var downArrow: UIImageView = {
        let img = UIImageView(image: #imageLiteral(resourceName: "downArrow"))
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFit
        return img
    }()
    
    var filterView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5
        view.isUserInteractionEnabled = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .primary
        return view
    }()
    
    var filterTapArea: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        return view
    }()
    
    var filterTap: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer()
        tap.numberOfTapsRequired = 1
        return tap
    }()
    
    var headLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Also look at\nYour might be interested"
        label.font = UIFont.systemFont(ofSize: 13)
        label.numberOfLines = 0
        label.textColor = UIColor(hexString: "FAFAFA")
        return label
    }()
    
    var isFilterExpanded: Bool = false
    var zoomTransition: ZoomTransition?
    var zoomTransitionThumbnail: UIImageView?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initial()
        navigationController?.setNavigation()
        title = "Movies"
        setupNav()
        setupCollections()
        setupUI()
        presenter.attachView(self)
        presenter.getMovies()
        presenter.getYears()
        presenter.getCachedMovies()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func setupNav() {
        let resetItem = UIBarButtonItem(image: #imageLiteral(resourceName: "reset"), style: .plain, target: self, action: #selector(resetClick))
        self.navigationItem.setRightBarButton(resetItem, animated: true)
    }
    
    @objc func resetClick() {
        let alert = UIAlertController(title: "Reset", message: "Are you sure to reset your content", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { _ in
            UD().resetAll()
            self.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { _ in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func didClickOnFilter(_ sender: UITapGestureRecognizer) {
        
        let view = sender.location(in: filterView)
        if view.y <= 35 {
            toggleFilter()
        }
    }
    
    func toggleFilter() {
        isFilterExpanded = !isFilterExpanded
        yearsCollectionView.isHidden = !isFilterExpanded
        ratingView.isHidden = !isFilterExpanded
        keywordsCollectionView.isHidden = !isFilterExpanded
        cachedMoviesCollection.isHidden = !isFilterExpanded
        refreshFilterView()
    }
    
    func refreshFilterView() {
        let height: CGFloat = UD().getLikedMovies().count != 0 ? 380 : 210
        filterView.snp.updateConstraints { builder in
            builder.height.equalTo(isFilterExpanded ? height : 60)
        }
        downArrow.transform = isFilterExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : .identity
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.90, initialSpringVelocity: 0.90, options: [], animations: {
            self.view.layoutIfNeeded()
            self.filterView.layoutIfNeeded()
        }, completion: nil)
    }
}
