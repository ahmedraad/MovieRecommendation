//
//  FilterTagCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/16/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class FilterTagCell: UICollectionViewCell {

    @IBOutlet weak var tagItem: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1
    }
    
    // Configure for year collection
    func configure(_ object: Year) {
        self.tagItem.text = object.title
        if let status = object.isSelected {
            if status {
                self.backgroundColor = .background
            } else {
                self.backgroundColor = .clear
            }
        } else {
            self.backgroundColor = .clear
        }
    }
    
    // Configure for keywords collection
    func configure(_ object: Tag) {
        self.tagItem.text = object.title
    }

}
