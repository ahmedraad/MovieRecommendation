//
//  MBasicDetailCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class MBasicDetailCell: UITableViewCell {
    
    @IBOutlet weak var bg: MaterialShadow!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var budget: UILabel!
    
    @IBOutlet weak var youtubeButton: MButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tagline.adjustsFontSizeToFitWidth = true
        poster.layer.cornerRadius = 5
        bg.backgroundColor = .lightPrimary
        bg.shadow_Color = .lightPrimary
    }
    
    func configure(_ object: Movie?) {
        guard let movie = object else { return }
        self.title.text = movie.title
        self.tagline.text = movie.tagline
        self.poster.kf.setImage(with: movie.poster)
        self.rate.text = "\(movie.imdb_rating) / 10"
        self.duration.text = "\(movie.runtime ?? 0) Min - \(movie.year)"
        if let b = movie.budget {
            let price = b as NSNumber
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            self.budget.text = "Budget \(formatter.string(from: price) ?? "N/A")"
        }
        
    }
    
}
