//
//  MActorCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class MActorCell: UICollectionViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var shadow: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        avatar.layer.cornerRadius = avatar.frame.width / 2
        avatar.clipsToBounds = true
        shadow.layer.cornerRadius = shadow.frame.width / 2
        shadow.clipsToBounds = true
    }
    
    func configure(_ object: Cast) {
        self.name.text = object.name
        self.avatar.kf.setImage(with: object.profileURL, placeholder: #imageLiteral(resourceName: "userPlaceholder"))
    }

}
