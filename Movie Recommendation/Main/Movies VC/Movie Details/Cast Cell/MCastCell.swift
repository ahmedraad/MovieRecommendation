//
//  MCastCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class MCastCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var bg: MaterialShadow!
    @IBOutlet weak var castCollection: UICollectionView!
    
    var cast = [Cast]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        castCollection.delegate = self
        castCollection.dataSource = self
        castCollection.registerNib("MActorCell")
        bg.backgroundColor = .lightPrimary
        bg.shadow_Color = .lightPrimary
    }
    
    func configure(_ object: Movie?) {
        guard let movie = object else { return }
        self.cast = movie.cast ?? []
        self.castCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MActorCell", for: indexPath) as! MActorCell
        cell.configure(cast[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 94, height: 124)
    }
    
}
