//
//  CrewPersonCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/22/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class CrewPersonCell: UITableViewCell {
    
    @IBOutlet weak var c_profile: UIImageView!
    @IBOutlet weak var c_title: UILabel!
    @IBOutlet weak var c_job: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        c_profile.layer.cornerRadius = c_profile.frame.width / 2
        c_profile.clipsToBounds = true
    }
    
    func configure(_ object: Crew) {
        c_title.text = object.name
        c_job.text = "\(object.job) - \(object.department)"
        c_profile.kf.setImage(with: object.profileURL, placeholder: #imageLiteral(resourceName: "userPlaceholder"))
    }
    
}
