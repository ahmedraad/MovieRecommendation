//
//  CrewCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/22/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class CrewCell: UITableViewCell {
    
    @IBOutlet weak var crewTable: UITableView!
    var crews = [Crew]()
    
    @IBOutlet weak var bg: MaterialShadow!
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTable()
        bg.backgroundColor = .lightPrimary
        bg.shadow_Color = .lightPrimary
    }
    
    func setupTable() {
        self.crewTable.delegate = self
        self.crewTable.dataSource = self
        self.crewTable.registernib("CrewPersonCell")
    }
    
    func configure(_ object: Movie?) {
        guard let movie = object else { return }
        self.crews = movie.crew ?? []
        self.crewTable.reloadData()
        let height: CGFloat = self.crewTable.contentSize.height
        self.tableHeight.constant = height
    }
    
}

extension CrewCell: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return crews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CrewPersonCell", for: indexPath) as! CrewPersonCell
        cell.configure(self.crews[indexPath.row])
        return cell
    }
}
