//
//  MDescriptionCell.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class MDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var bg: MaterialShadow!
    @IBOutlet weak var descriptionText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bg.backgroundColor = .lightPrimary
        bg.shadow_Color = .lightPrimary
    }

    func configure(_ objcet: Movie?) {
        guard let movie = objcet else { return }
        self.descriptionText.text = movie.overview
    }
    
}
