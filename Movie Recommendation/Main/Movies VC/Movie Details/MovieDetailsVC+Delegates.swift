//
//  MovieDetailsVC+Delegates.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import FSPagerView
import UIKit
import Kingfisher

extension MovieDetailsVC: FSPagerViewDelegate, FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return currentMovie?.fullImages().count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        let info = KingfisherOptionsInfo(arrayLiteral: .transition(.fade(1)))
        cell.imageView?.kf.indicatorType = .activity
        cell.imageView?.kf.indicator?.startAnimatingView()
        let url = URL(string: self.currentMovie?.fullImages()[index] ?? "")
        cell.imageView?.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "sliderPlaceholder"), options: info, progressBlock: nil, completionHandler: { (_, _, _, _) in
            cell.imageView?.kf.indicator?.stopAnimatingView()
        })
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MBasicDetailCell", for: indexPath) as! MBasicDetailCell
            cell.configure(currentMovie)
            cell.youtubeButton.addTarget(self, action: #selector(youtubeClick), for: .touchUpInside)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MDescriptionCell", for: indexPath) as! MDescriptionCell
            cell.configure(currentMovie)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MCastCell", for: indexPath) as! MCastCell
            cell.configure(currentMovie)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CrewCell", for: indexPath) as! CrewCell
            cell.configure(currentMovie)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let yOffset = scrollView.contentOffset.y
        let xOffset = (CGFloat(yOffset) + topViewHeight) / 2
        if yOffset >= -20 {
            if isX {
                self.blurView.frame = CGRect(x: 0, y: yOffset, width: self.tableView!.frame.size.width, height: 44)
            } else {
                self.blurView.frame = CGRect(x: 0, y: yOffset, width: self.tableView!.frame.size.width, height: 22)
            }
            self.blurView.alpha = 1.0
        } else {
            self.blurView.alpha = 0.0
        }
        
        if (yOffset < -topViewHeight) {
            
            var rect = self.topImageView.frame
            rect.origin.y = yOffset
            rect.size.height = -yOffset
            rect.origin.x = xOffset
            rect.size.width = scrollView.frame.size.width + fabs(xOffset)*2
            self.infoView.snp.updateConstraints({ builder in
                builder.width.equalTo(topImageView).offset(xOffset * 2.0)
            })
            
            self.topImageView.frame = rect
        }
    }
    
}
