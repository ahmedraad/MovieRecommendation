//
//  MovieDetailsVC+Requests.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

extension MovieDetailsVC {
    
    func getMovie() {
        guard let id = self.currentMovie?.id else {return}
        network.request(type: .get ,route: .movie, parameters: [:], movie_id: id, isFilter: false) { (data) in
            do {
                let json = try JSONDecoder().decode(Movie.self, from: data)
                self.currentMovie = json
                self.tableView.reloadData()
                self.checkPurchases()
                self.topImageView.reloadData()
            } catch {
                self.showNetworkMessage(error.localizedDescription)
            }
        }
    }
}
