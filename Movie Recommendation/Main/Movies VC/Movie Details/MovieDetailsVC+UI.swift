//
//  MovieDetailsVC+UI.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation
import UIKit
import FSPagerView
import ChameleonFramework

extension MovieDetailsVC {
    
    func setupTable() {
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        self.tableView.registernib("MBasicDetailCell")
        self.tableView.registernib("MDescriptionCell")
        self.tableView.registernib("MCastCell")
        self.tableView.registernib("CrewCell")
    }
    
    func setupHeader() -> UIView {
        self.tableView.showsVerticalScrollIndicator = false
        self.tableView?.contentInset = UIEdgeInsets(top: topViewHeight, left: 0, bottom: 0, right: 0)
        let topView = FSPagerView()
        topView.backgroundColor = .lightPrimary
        topView.frame = CGRect(x: 0, y: -topViewHeight, width: self.tableView!.frame.size.width, height: topViewHeight)
        topView.delegate = self
        topView.dataSource = self
        topView.automaticSlidingInterval = 6
        topView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.tableView.addSubview(topView)
        
        self.view.addSubview(blurView)
        
        var height: CGFloat = 22
        if isX { height = 44 }
        blurView.frame = CGRect(x: 0, y: 0,width: self.tableView!.frame.size.width, height: height)
        
        topView.addSubview(infoView)
        
        infoView.snp.makeConstraints { builder in
            builder.height.equalTo(50)
            builder.bottom.equalTo(topView)
            builder.width.equalTo(topView)
            builder.centerX.equalTo(topView)
        }
        
        self.topImageView = topView
        return topView
    }
    
    func setupUI() {
        let topView = setupHeader()
        
        topView.addSubview(self.closeButton)
        closeButton.snp.makeConstraints { builder in
            if isX {
                builder.top.equalTo(topView).offset(35)
                builder.leading.equalTo(topView).offset(35)
                builder.height.width.equalTo(40)
            } else {
                builder.top.equalTo(topView).offset(25)
                builder.leading.equalTo(topView).offset(15)
                builder.height.width.equalTo(40)
            }
        }
        
        setupButtons()
    }
    
    func setupButtons() {
        infoView.addSubview(itunesButton)
        itunesButton.snp.makeConstraints { builder in
            builder.bottom.equalTo(infoView.snp.bottom).offset(-10)
            builder.width.equalTo(90)
            builder.height.equalTo(45)
            builder.leading.equalTo(infoView).offset(15)
        }
        
        infoView.addSubview(amazonButton)
        amazonButton.snp.makeConstraints { builder in
            builder.bottom.equalTo(infoView.snp.bottom).offset(-10)
            builder.width.equalTo(90)
            builder.height.equalTo(45)
            builder.leading.equalTo(itunesButton.snp.trailing).offset(15)
        }
        
        
    }
    
    func checkPurchases() {
        if self.currentMovie?.itunes != nil {
            self.itunesButton.isEnabled = true
        } else {
            self.itunesButton.isEnabled = false
        }
        if self.currentMovie?.amazon != nil {
            self.amazonButton.isEnabled = true
        } else {
            self.amazonButton.isEnabled = false
        }
    }
}
