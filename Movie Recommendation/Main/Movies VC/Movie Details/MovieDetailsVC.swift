//
//  MovieDetailsVC.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit
import FSPagerView
import SafariServices

class MovieDetailsVC: UITableViewController {
    
    var currentMovie: Movie?
    
    var closeButton: MButton = {
        let button = MButton()
        button.isCircle = true
        button.color = .darkWhite
        button.image_inset = 10
        button.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        return button
    }()
    
    var itunesButton: MButton = {
        let btn = MButton()
        btn.isEnabled = false
        btn.isCircle = false
        btn.color = .clear
        btn.image_inset = 0
        btn.alpha = 0.9
        btn.setImage(#imageLiteral(resourceName: "enabledItunes"), for: .normal)
        btn.addTarget(self, action: #selector(didClickItunes), for: .touchUpInside)
        return btn
    }()
    
    var amazonButton: MButton = {
        let btn = MButton()
        btn.isEnabled = false
        btn.isCircle = false
        btn.color = .clear
        btn.image_inset = 0
        btn.alpha = 0.9
        btn.setImage(#imageLiteral(resourceName: "enabledAmazon"), for: .normal)
        btn.addTarget(self, action: #selector(didClickAmz), for: .touchUpInside)
        return btn
    }()
    
    var infoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var blurView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: .light)
        let view = UIVisualEffectView(effect: effect)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 0.0
        return view
    }()
    
    var topImageView = FSPagerView()
    let topViewHeight: CGFloat = 300

    override func viewDidLoad() {
        super.viewDidLoad()
        initial()
        setupUI()
        setupTable()
        getMovie()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func dismissView() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func youtubeClick() {
        guard let trailer = self.currentMovie?.trailer else { return }
        if let url = URL(string: "https://youtube.com/watch?v=\(trailer)") {
            let sf = SFSafariViewController(url: url)
            UIApplication.shared.statusBarStyle = .default
            self.present(sf, animated: true, completion: nil)
        }
    }
    
    @objc func didClickItunes() {
        guard let object = self.currentMovie, let itunes = object.itunes else {
            return
        }
        
        if let url = URL(string: "https://itunes.apple.com/us/movie/leap/id\(itunes.trackId)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    @objc func didClickAmz() {
        guard let object = self.currentMovie, let amz = object.amazon else {
            return
        }
        
        if let url = URL(string: amz.link) {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }

}
