//
//  SplashVC.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/28/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    var logo: UIImageView = {
        let img = UIImageView(image: #imageLiteral(resourceName: "logo"))
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFit
        return img
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initial()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
        checkUserData()
    }
    
    func setupUI() {
        view.addSubview(logo)
        logo.snp.makeConstraints { builder in
            builder.leading.trailing.equalTo(view)
            builder.centerY.equalTo(view)
            builder.height.equalTo(200)
        }
    }

    func presentMovies() {
        let vc = MoviesVC()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalTransitionStyle = .crossDissolve
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    
    func presentCategories() {
        let vc = StartVC()
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func checkUserData() {
        if UD().requestPara().count > 0 {
            delay(0.8, completion: {
                self.presentMovies()
            })
        } else {
            delay(0.8, completion: {
                self.presentCategories()
            })
        }
    }
}
