//
//  StartVC.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/7/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import UIKit

class StartVC: UIViewController {
    
    var headTitle: UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.text = "Find a perfect match to your mood"
        lb.textAlignment = .center
        lb.textColor = .white
        lb.font = UIFont.systemFont(ofSize: 13, weight: .bold)
        return lb
    }()
    
    var continueButton: MovieButton = {
        let btn = MovieButton(type: .custom)
        btn.setTitle("Continue", for: .normal)
        btn.isEnabled = false
        btn.addTarget(self, action: #selector(continueClicked), for: .touchUpInside)
        return btn
    }()
    
    var categoriesCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        var collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.translatesAutoresizingMaskIntoConstraints = false
        collection.backgroundColor = .clear
        collection.showsVerticalScrollIndicator = false
        return collection
    }()
    
    var categories = [Tag]()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initial()
        setupUI()
        setupCollection()
        getCategories()
    }
    
    func setupUI() {
        let sideConstraints: CGFloat = 20
        
        view.addSubview(headTitle)
        headTitle.snp.makeConstraints { builder in
            if isX { builder.top.equalTo(view).offset(40) } else { builder.top.equalTo(view).offset(20) }
            builder.leading.equalTo(view).offset(sideConstraints)
            builder.trailing.equalTo(view).offset(-sideConstraints)
        }
        
        view.addSubview(continueButton)
        continueButton.snp.makeConstraints { builder in
            if isX { builder.bottom.equalTo(view).offset(-30) } else { builder.bottom.equalTo(view).offset(-20) }
            builder.leading.equalTo(view).offset(sideConstraints)
            builder.trailing.equalTo(view).offset(-sideConstraints)
            builder.height.equalTo(45)
        }
        
        view.addSubview(categoriesCollection)
        categoriesCollection.snp.makeConstraints { builder in
            builder.top.equalTo(headTitle.snp.bottomMargin).offset(20)
            builder.leading.equalTo(view).offset(sideConstraints)
            builder.trailing.equalTo(view).offset(-sideConstraints)
            builder.bottom.equalTo(continueButton.snp.topMargin).offset(-20)
        }
    }
    
    
    func setupCollection() {
        categoriesCollection.delegate = self
        categoriesCollection.dataSource = self
        categoriesCollection.registerNib("CategoryCell")
    }
    
    func getCategories() {
        guard let fileUrl = Bundle.main.url(forResource: "Categories", withExtension: "plist") else {
            print("error reading file")
            return
        }
        do {
            guard let data = try? Data(contentsOf: fileUrl) else {
                print("error reading data")
                return
            }
            let json = try PropertyListDecoder().decode([Tag].self, from: data)
            self.categories = json
            self.categoriesCollection.reloadData()
        } catch {
            print(error)
        }
    }
    
    func checkCategoriesStatus() {
        let status = self.categories.contains(where: { return $0.isSelected ?? false })
        self.continueButton.isEnabled = status
        UD().setUserData(self.categories)
    }
    
    @objc func continueClicked() {
        presentMovies()
    }
    
    func presentMovies() {
        let vc = MoviesVC()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalTransitionStyle = .crossDissolve
        UIApplication.shared.statusBarStyle = .lightContent
        self.present(nav, animated: true, completion: nil)
    }
    
}

extension StartVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.configure(self.categories[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let status = categories[indexPath.item].isSelected {
            if status {
                self.categories[indexPath.item].toggle(false)
            } else {
                self.categories[indexPath.item].toggle(true)
            }
        } else {
            self.categories[indexPath.item].toggle(true)
        }
        self.categoriesCollection.reloadData()
        self.checkCategoriesStatus()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size: CGSize = CGSize(width: 0, height: 0)
        if  UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad {
            size = CGSize(width: self.categoriesCollection.bounds.size.width / 4, height: self.categoriesCollection.bounds.size.width / 5)
        } else {
            size = CGSize(width: self.categoriesCollection.bounds.size.width / 3, height: self.categoriesCollection.bounds.size.width / 4)
        }
        return size
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        categoriesCollection.collectionViewLayout.invalidateLayout()
    }
}
