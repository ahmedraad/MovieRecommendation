//
//  Cast.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/21/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

struct Cast: Codable {
    
    var cast_id: Int
    var character: String
    var credit_id: String
    var id: Int
    var name: String
    var profile_path: String?
    
    var profileURL: URL? {
        if let prof = profile_path {
            return URL(string: "https://image.tmdb.org/t/p/w138_and_h175_bestv2" + prof)
        }
        return nil
    }
}
