//
//  Crew.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/22/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

struct Crew: Codable {
    
    var credit_id: String
    var department: String
    var id: Int
    var job: String
    var name: String
    var profile_path: String?
    
    var profileURL: URL? {
        if let prof = profile_path {
            return URL(string: "https://image.tmdb.org/t/p/w138_and_h175_bestv2" + prof)
        }
        return nil
    }
}
