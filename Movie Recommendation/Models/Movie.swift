//
//  Movie.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/7/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

struct Response: Codable {
    
    var items: [Movie] = []
}

struct Movie: Codable {
    
    var id: String
    var title: String
    var overview: String
    var year: String
    var imdb_rating: String
    var poster_path: String?
    var imdb_id: String?
    var trailer: String?
    var budget: UInt?
    var tagline: String?
    var images: [String]?
    var runtime: Int?
    var itunes: Itunes?
    var amazon: Amazon?
    var tags = [Tag]()
    var cast: [Cast]? = []
    var crew: [Crew]? = []
    
    var isLiked: Bool?
    
    var poster: URL? {
        return URL(string: "https://image.tmdb.org/t/p/w185_and_h278_bestv2\(poster_path ?? "")")
    }
    
    func fullImages() -> [String] {
        var imgs: [String] = []
        self.images?.forEach { imgs.append("https://image.tmdb.org/t/p/w1280" + $0) }
        return imgs
    }
}

struct Itunes: Codable {
    
    var trackHdPrice: Double
    var trackPrice: Double
    var trackId: Int
}

struct Amazon: Codable {
    
    var asin: String
    var link: String
}
