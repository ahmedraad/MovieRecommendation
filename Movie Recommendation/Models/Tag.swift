//
//  Tag.swift
//  Movie Recommendation
//
//  Created by Ahmed Raad on 11/7/17.
//  Copyright © 2017 Ahmed Raad. All rights reserved.
//

import Foundation

struct Tag: Codable {
    
    var id: String
    var title: String
    var isSelected: Bool? = false
    
    mutating func toggle(_ status: Bool) {
        self.isSelected = status
    }
}

struct Year: Codable {
    
    var title: String
    var value: String
    var isSelected: Bool? = false
    
    mutating func toggle(_ status: Bool) {
        self.isSelected = status
    }
}
